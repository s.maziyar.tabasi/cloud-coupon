import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from '../user/entities/user.entity';
import { DiscountType } from '../user/entities/discount.entity';

export enum CouponType {
  CREDIT = 'credit',
  DISCOUNT = 'discount',
}

@Entity()
export class Coupon {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  code: string;
  @Column()
  type: CouponType;
  @Column()
  value: number;
  @Column()
  capacity: number;
  @Column({ nullable: true })
  discountType: DiscountType;
  @ManyToMany(() => User, (user) => user.coupons)
  @JoinTable()
  users: User[];
}

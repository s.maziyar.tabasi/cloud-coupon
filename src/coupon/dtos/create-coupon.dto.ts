import { CouponType } from '../coupon.entity';
import { IsEnum, IsNumber, Min, IsOptional } from 'class-validator';
import { DiscountType } from '../../user/entities/discount.entity';

export class CreateCouponDto {
  @IsEnum(CouponType)
  type: CouponType;
  @IsNumber()
  @Min(0)
  value: number;
  @IsNumber()
  @Min(0)
  capacity: number;
  @IsEnum(DiscountType)
  @IsOptional()
  discountType: DiscountType;
}

import { IsString } from 'class-validator';

export class UseCouponDto {
  @IsString()
  code: string;
}

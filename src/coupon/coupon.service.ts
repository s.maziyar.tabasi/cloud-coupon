import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coupon, CouponType } from './coupon.entity';
import { Repository } from 'typeorm';
import { errorMessages } from './coupon.constants';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import { DiscountType } from '../user/entities/discount.entity';

const scrypt = promisify(_scrypt);

@Injectable()
export class CouponService {
  constructor(
    @InjectRepository(Coupon) private repo: Repository<Coupon>,
    private userService: UserService,
  ) {}

  async useCoupon(code: string, user: User) {
    const coupon = await this.findCouponByCode(code);
    if (coupon.capacity === 0) {
      throw new BadRequestException(errorMessages.NO_CAPACITY);
    }
    if (await this.isCouponUsed(coupon, user)) {
      throw new BadRequestException(errorMessages.COUPON_ALREADY_USED);
    }

    coupon.capacity--;
    coupon.users = [...(await this.findCouponUsers(coupon)), user];
    await this.repo.save(coupon);

    if (coupon.type === CouponType.CREDIT) {
      return this.userService.addCredit(user, coupon);
    }
    if (coupon.type === CouponType.DISCOUNT) {
      return this.userService.addDiscount(user, coupon);
    }
  }

  async createCoupon(
    capacity: number,
    type: CouponType,
    value: number,
    discountType: DiscountType,
  ) {
    const code = (await scrypt(
      Date.now().toString(),
      randomBytes(4),
      10,
    )) as Buffer;
    const coupon = this.repo.create({
      code: code.toString('hex'),
      capacity,
      type,
      value,
      discountType,
    });
    return this.repo.save(coupon);
  }

  async findCouponById(id: number) {
    const coupon = await this.repo.findOneBy({ id });
    if (!coupon) {
      throw new NotFoundException(errorMessages.COUPON_NOT_FOUND);
    }
    return coupon;
  }

  async findCouponByCode(code: string) {
    const coupon = await this.repo.findOneBy({ code });
    if (!coupon) {
      throw new BadRequestException(errorMessages.COUPON_NOT_FOUND);
    }
    return coupon;
  }

  async isCouponUsed(coupon: Coupon, user: User) {
    const users = await this.findCouponUsers(coupon);
    const userIds = users.map((user) => user.id);
    return userIds.filter((userId) => userId === user.id).length === 1;
  }

  async findCouponUsers(coupon: Coupon) {
    const [{ users }] = await this.repo
      .createQueryBuilder('coupon')
      .where('coupon.id = :id', { id: coupon.id })
      .leftJoinAndSelect('coupon.users', 'user')
      .getMany();
    return users;
  }

  findAllCoupons() {
    return this.repo.find();
  }
}

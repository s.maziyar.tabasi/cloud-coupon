import {
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CreateCouponDto } from './dtos/create-coupon.dto';
import { AdminGuard } from '../guards/admin.guard';
import { CouponService } from './coupon.service';
import { errorMessages } from './coupon.constants';
import { AuthGuard } from '../guards/auth.guard';
import { CurrentUser } from '../user/decorators/current-user.decorator';
import { User } from '../user/entities/user.entity';
import { UseCouponDto } from './dtos/use-coupon.dto';
import { Serialize } from '../interceptors/serialize.interceptor';
import { UserDto } from '../user/dtos/user.dto';

@Controller('coupons')
export class CouponController {
  constructor(private couponService: CouponService) {}

  @Post('/use')
  @UseGuards(AuthGuard)
  @Serialize(UserDto)
  useCoupon(@Query() query: UseCouponDto, @CurrentUser() user: User) {
    return this.couponService.useCoupon(query.code, user);
  }

  @Post()
  @UseGuards(AdminGuard)
  createCoupon(@Body() body: CreateCouponDto) {
    return this.couponService.createCoupon(
      body.capacity,
      body.type,
      body.value,
      body.discountType,
    );
  }

  @Get('/:id')
  @UseGuards(AdminGuard)
  async findCoupon(@Param('id') id: string) {
    const coupon = await this.couponService.findCouponById(Number(id));
    if (!coupon) {
      throw new NotFoundException(errorMessages.COUPON_NOT_FOUND);
    }
    return coupon;
  }

  @Get()
  @UseGuards(AdminGuard)
  findAllCoupons() {
    return this.couponService.findAllCoupons();
  }
}

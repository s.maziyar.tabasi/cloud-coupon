export const errorMessages = {
  COUPON_NOT_FOUND: 'coupon not found',
  NO_CAPACITY: 'coupon dose not have capacity',
  COUPON_ALREADY_USED: 'coupon already used',
};

export const errorMessages = {
  IN_USE_EMAIL: 'email already in use',
  INCORRECT_EMAIL_OR_PASSWORD: 'incorrect email or password',
  USER_NOT_FOUND: 'user not found',
};

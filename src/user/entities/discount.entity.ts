import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

export enum DiscountType {
  DEDICATE = 'dedicate',
  VIRTUAL = 'virtual',
  CLOUD = 'cloud',
}

@Entity()
export class Discount {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  type: DiscountType;
  @Column()
  value: number;
  @ManyToOne(() => User, (user) => user.discounts)
  user: User;
}

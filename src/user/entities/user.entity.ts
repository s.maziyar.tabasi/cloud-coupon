import {
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Coupon } from '../../coupon/coupon.entity';
import { Discount } from './discount.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  email: string;
  @Column()
  password: string;
  @Column({ default: false })
  isAdmin: boolean;
  @Column({ default: 0 })
  credit: number;
  @ManyToMany(() => Coupon, (coupon) => coupon.users)
  coupons: Coupon[];
  @OneToMany(() => Discount, (discount) => discount.user)
  discounts: Discount[];
}

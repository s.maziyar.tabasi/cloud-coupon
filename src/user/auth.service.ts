import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from './user.service';
import { errorMessages } from './user.constants';
import { randomBytes, scrypt as _scrypt } from 'crypto';
import { promisify } from 'util';

const scrypt = promisify(_scrypt);

@Injectable()
export class AuthService {
  constructor(private userService: UserService) {}

  async signup(email: string, password: string) {
    const user = await this.userService.findByEmail(email);
    if (user) {
      throw new BadRequestException(errorMessages.IN_USE_EMAIL);
    }
    const salt = randomBytes(8).toString('hex');
    const hash = (await scrypt(password, salt, 32)) as Buffer;
    const hashedPassword = `${salt}.${hash.toString('hex')}`;
    return this.userService.create(email, hashedPassword);
  }

  async signin(email: string, password: string) {
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new BadRequestException(errorMessages.INCORRECT_EMAIL_OR_PASSWORD);
    }
    const [salt, storedHash] = user.password.split('.');
    const hash = (await scrypt(password, salt, 32)) as Buffer;
    if (hash.toString('hex') !== storedHash) {
      throw new BadRequestException(errorMessages.INCORRECT_EMAIL_OR_PASSWORD);
    }
    return user;
  }
}

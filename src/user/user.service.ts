import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { Coupon } from '../coupon/coupon.entity';
import { Discount } from './entities/discount.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(Discount) private discountRepo: Repository<Discount>,
  ) {}

  findByEmail(email: string) {
    return this.userRepo.findOneBy({ email });
  }

  findById(id: number) {
    return this.userRepo.findOneBy({ id });
  }

  create(email: string, password: string) {
    const user = this.userRepo.create({ email, password });
    return this.userRepo.save(user);
  }

  async addDiscount(user: User, coupon: Coupon) {
    const discount = this.discountRepo.create({
      type: coupon.discountType,
      value: coupon.value,
      user,
    });
    await this.discountRepo.save(discount);
    return user;
  }

  addCredit(user: User, coupon: Coupon) {
    user.credit += coupon.value;
    return this.userRepo.save(user);
  }
}

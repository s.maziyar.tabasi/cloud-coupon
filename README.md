## Description

The request examples are in ***cloud coupon.postman_collection.json***. It can be imported in postman.
The SQLite database is used for simplicity and there is some seed data in it.
some routes need admin authorization the user and password for admin are:

- user: admin@admin.com

- password: admin

I assume that there is two type of coupons:

- credit
- discount

The discount coupon has three type that use for discounting the value of servers.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
```

- Author - Seyed Maziyar Tabasi

